# Raspberry PI Install from scratch

<img src="icon.png" width="256" height="256" alt="Mac Dev Playbook Logo" />

_Easy installation icons created by [Freepik - Flaticon](https://www.flaticon.com/free-icons/easy-installation)_

This project automates Raspberry PI (raspi) operating system installation and basic setup (user
creation, password change, etc) for use as a home lab server. Work inspired by Jeff Geerling's
[Mac Development Ansible Playbook](https://github.com/geerlingguy/mac-dev-playbook/)

## Available automated tasks

* _Micro SD image transfer_: transfers a downloaded OS image to a micro SD card in your setup machine
* _Initial password change_: changes the default password most OSes use for the default account
* _Packages update_: you know how important is to use the most up to date versions of each setup
* _Copy your ssh credentials_: transfers your setup machine default ssh creadentials to raspi for easier access and administration
* _Add new users_: fast and simple
* _Mount file systems in memory_: minimize micro SD read/write cycle waste (this is how this prject started :P)

This is not a one shot process, you can run this tasks just to update your install
(add new users, update them, copy new credentials) instead of running them once when installing your shiny new raspi

_Given that the project was developed in a Linux box and used to install Ubuntu Linux in the
micro SD be warned that some bias can exist. Feel free to create a fix and
[submit a merge request](https://gitlab.com/the-automation-club/raspi-install) or
[fill a bug](https://gitlab.com/the-automation-club/raspi-install/-/issues/new) explaining the issue
and how you expect it should work_

## Preparation

- Install [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/index.html)
- Install [sshpass](https://www.tecmint.com/sshpass-non-interactive-ssh-login-shell-script-ssh-password/)

## How to run

This project is divided in two: `microsd-image` for OS installation only and
`live-setup` to setup OS use once raspi has been boot

- _OS install_ : insert micro SD card in your setup machine and run `cd microsd-image && ansible-playbook main.yml --ask-become-pass`
- _Raspi setup_: insert micro SD into your raspi, power it up and in yor setup machine run `cd live-setup && ansible-playbook main.yml --ask-become-pass`

Run all commands with
[--ask-become-pass](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_privilege_escalation.html#understanding-privilege-escalation-become)
and enter your sudo password when prompted for the 'BECOME' password otherwise it won't work

## Running setup parts

Sometimes one part fails or simply you can change one part of your install and not the whole
enchilada. You can filter which part of the setup process to run by specifying a set of tags
using

`ansible-playbook main.yml --list-tags`

and run the one you choose like :

`ansible-playbook main.yml --ask-become-pass --tag=add-users`

## Customizing

The file `default.config.yml` are example settings and can be overridden by creating
`override/config.yml` and setting the overrides in this file (any file under `override` folder will be hidden from git)

```yaml
## [Micro SD image]
micro_sd_device: /dev/sda
# OS image to write into micro SD card. Currently accepting filesystem paths only
os_image_location: "{{ lookup('env','HOME') }}/Downloads/ubuntu-22.04.2-preinstalled-server-arm64+raspi.img"

## [Live setup]
# Time to wait for raspi startup (in secs.)
wait_for_raspi_timeout: 300

# - Login credentials
# user : ssh login user
# password : default password. At install time some OSes create a default user with a default password
# new_password : 'password' will be changed by this one
raspi_login: {
    user: ubuntu,
    password: ubuntu,
    new_password: p4ssw0rd
}

# Users to be created. Be warned that for more than one group to be assigned
# the setting 'groups' must be double quoted (e.g. in user2 groups)
new_users:
- { name: user1, shell: /bin/bash, groups: adm }
- { name: user2, shell: /bin/sh, groups: "adm,audio" }

# Filesystems to be mounted in memory (tmpfs) to maximize micro SD life time because of
# limited erase/write cycles
tmpfs:
- { mount: /tmp2, size: 10M }
- { mount: /tmp3, size: 100M }

# sshd configuration, written to /etc/ssh/sshd_config.d/00-my-settings.conf
sshd_config:
    - { key: "PubkeyAuthentication", value: "yes" }
    - { key: "PasswordAuthentication", value: "no"}
```
